<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<head>

<meta charset="<?php bloginfo( 'charset' ); ?>">
	 <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Strona %s', '' ), max( $paged, $page ) );

	?></title>

	
	
<link href="<?php echo get_template_directory_uri(); ?>/style.css" rel="stylesheet" />
<link href="<?php echo get_template_directory_uri(); ?>/style2.css" rel="stylesheet" />


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">  </script>    
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js">  </script>


<script type="text/javascript">




$(function() {
	$('#boczne > ul > li > a').click(function(){
	$('#boczne').fadeOut(300)	
    $('html, body').animate({
        scrollTop: $( $(this).attr('href') ).offset().top
    }, 800);
    return false;
	
});

	$('.boxy a').click(function(){
	$('html, body').animate({
        scrollTop: $( $(this).attr('href') ).offset().top
    }, 800);
    return false;
	
});


$('.kolo').click(function() {
                $('#boczne').fadeToggle(300);
        });
	$('#wrap').click(function(){
		$('#boczne').fadeOut(300);
	})
	$('#boczne').click(function(){
		$('#boczne').fadeOut(300);
	})
	
});
</script>

<link href="<?php echo get_template_directory_uri(); ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<script src="<?php echo get_template_directory_uri(); ?>/bootstrap/js/bootstrap.min.js"></script>

<?php wp_head(); ?>
</head>
 <body>
 <!--<div id="sorry">
 <h1> Przepraszamy, strona chwilowo niedostępna. </div>
 </div>-->
 
 <div id="menu">
 
<div class="logo_baner col-lg-12 col-md-12 col-sm-12 col-xs-12">
		
		<div class="logo_baner1 col-lg-5 col-md-5 col-sm-5 col-xs-10">
			<img class=" img-responsive logo1" src="<?php echo get_template_directory_uri(); ?>/img/logo_baner.png" alt="ecoram.eu" />
		</div>
		
		<div class="logo_baner2  col-lg-5 col-md-5 col-sm-5 col-xs-12">
		<!--<a href="http://www.zksizolator.eu" title="Izolator Boguchwała" target="_blank">
			<img class=" img-responsive sponsor" src="<?php echo get_template_directory_uri(); ?>/img/izo_nowe.png" alt="sponsor"   >
		</a>	<!-- <p class="col-lg-6 col-md-6 col-sm-6 col-xs-6"> Wspieramy Izolator Boguchwała</p> -->
		</div>
		
		<div id="wysun" class=" col-lg-2 col-md-2 col-sm-2 col-xs-2" ><div class="kolo"><p>&#9776; Menu</p></div> </div>
</div>
		
		<div id="boczne"  class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
		
		<ul class="menu">	
				<li><a class="text-uppercase" href="#szyba" > Główna</a>
                </li>
				<li><a class="text-uppercase" href="#paliwa" >Paliwa</a>
                </li>
                <li><a class="text-uppercase" href="#pelet" >Pelet</a>
                </li>
                <li><a class="text-uppercase" href="#outsourcing"  >Outsourcing</a>
                </li>
                <li><a class="text-uppercase" href="#formularz"  >Kontakt</a>
                </li>
		</ul>
		
		
		</div>
		

</div>


<div id="wrap">
  <div class="row" id="szyba"> 

<div class="boxy">
<a href="#paliwa" class="box box-3 col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="tlo col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="box-text">
									<h2 class="text-center text-uppercase">Paliwa</h2>
									<p class="text-center"><span class="box-wiecej">więcej</span></p>
								</div>

						</div>
</a>

  
<a href="#pelet" class="box box-2 col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="tlo col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="box-text">
									<h2 class="text-center text-uppercase">Pelet</h2>
									<p class="text-center"><span class="box-wiecej">więcej</span></p>
								</div>

						</div>
</a>

 
 
<a href="#outsourcing" class="box box-1 col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="tlo col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="box-text">
									<h2 class="text-center text-uppercase">Outsourcing</h2>
									<p class="text-center"><span class="box-wiecej">więcej</span></p>
								</div>

						</div>
</a>
<a href="#formularz" class="box box-4 col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="tlo col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="box-text">
									<h2 class="text-center text-uppercase">Kontakt</h2>
									<p class="text-center"><span class="box-wiecej">więcej</span></p>
								</div>

						</div>
</a>
</div>
 </div>