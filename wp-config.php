<?php
/**
 * Podstawowa konfiguracja WordPressa.
 *
 * Skrypt wp-config.php używa tego pliku podczas instalacji.
 * Nie musisz dokonywać konfiguracji przy pomocy przeglądarki internetowej,
 * możesz też skopiować ten plik, nazwać kopię "wp-config.php"
 * i wpisać wartości ręcznie.
 *
 * Ten plik zawiera konfigurację:
 *
 * * ustawień MySQL-a,
 * * tajnych kluczy,
 * * prefiksu nazw tabel w bazie danych,
 * * ABSPATH.
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Ustawienia MySQL-a - możesz uzyskać je od administratora Twojego serwera ** //
/** Nazwa bazy danych, której używać ma WordPress */
define('DB_NAME', 'seanaview_3');

/** Nazwa użytkownika bazy danych MySQL */
define('DB_USER', 'seanaview_3');

/** Hasło użytkownika bazy danych MySQL */
define('DB_PASSWORD', 'Firma1234');

/** Nazwa hosta serwera MySQL */
define('DB_HOST', 'sql.seanaview.nazwa.pl');

/** Kodowanie bazy danych używane do stworzenia tabel w bazie danych. */
define('DB_CHARSET', 'utf8mb4');

/** Typ porównań w bazie danych. Nie zmieniaj tego ustawienia, jeśli masz jakieś wątpliwości. */
define('DB_COLLATE', '');

/**#@+
 * Unikatowe klucze uwierzytelniania i sole.
 *
 * Zmień każdy klucz tak, aby był inną, unikatową frazą!
 * Możesz wygenerować klucze przy pomocy {@link https://api.wordpress.org/secret-key/1.1/salt/ serwisu generującego tajne klucze witryny WordPress.org}
 * Klucze te mogą zostać zmienione w dowolnej chwili, aby uczynić nieważnymi wszelkie istniejące ciasteczka. Uczynienie tego zmusi wszystkich użytkowników do ponownego zalogowania się.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ']0Sp:?U&2uAX!u[Q1:qM.*uy6jer ouk(p#pDP]))hV_$GxUswMQ7DIEiWw!~l^3');
define('SECURE_AUTH_KEY',  '?965Aay@_/&#XHRc(x3|zS(TfJ6.kx@[Y]!MI&kY{t&|Rm;;U(|fTa e, &ByX k');
define('LOGGED_IN_KEY',    '% ^OvUVIbwJH=ymvb2_ YTMc23s<r:3#zOQ[[AU!XSjn2C&;{]sf*RnHcom6lw$j');
define('NONCE_KEY',        'Dpi$J{pX2l )WbC6n6c]RaPc]4Zhje.e@y~CEg,4>t@R+.%Lb*S$Z?x4KzWt}liU');
define('AUTH_SALT',        'czV|6gC?+dH(MrCWVk29D5-&d@%OHu5+twO^c/^FZ`=^iR2CTa3Mu3!t:nh5IC4I');
define('SECURE_AUTH_SALT', 'o90@ALN*OD0EY*[;oNGe)P^EgWM<hwW.JpS34w@c4!GMLB6BQfC3IIXHW^+/Pl|f');
define('LOGGED_IN_SALT',   '~~4}Fh7BvG#i, ~:p3Bo]r{roWl[(B(5~x*>)2l>YTN{9Qim*{&qd$,ZN2 9`}DZ');
define('NONCE_SALT',       '7iuHC*TZGV&{A4zT`1usj#Z=$J3lwN*C_HNy$6oR)$CG?UR.82#rqS#QV^&Og6,c');

/**#@-*/

/**
 * Prefiks tabel WordPressa w bazie danych.
 *
 * Możesz posiadać kilka instalacji WordPressa w jednej bazie danych,
 * jeżeli nadasz każdej z nich unikalny prefiks.
 * Tylko cyfry, litery i znaki podkreślenia, proszę!
 */
$table_prefix  = 'wp_';

/**
 * Dla programistów: tryb debugowania WordPressa.
 *
 * Zmień wartość tej stałej na true, aby włączyć wyświetlanie
 * ostrzeżeń podczas modyfikowania kodu WordPressa.
 * Wielce zalecane jest, aby twórcy wtyczek oraz motywów używali
 * WP_DEBUG podczas pracy nad nimi.
 *
 * Aby uzyskać informacje o innych stałych, które mogą zostać użyte
 * do debugowania, przejdź na stronę Kodeksu WordPressa.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* To wszystko, zakończ edycję w tym miejscu! Miłego blogowania! */

/** Absolutna ścieżka do katalogu WordPressa. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Ustawia zmienne WordPressa i dołączane pliki. */
require_once(ABSPATH . 'wp-settings.php');
